# `timer`

`timer` is a tiny golang utility that helps me track time in 25min chunks, inspired by pomodoro.

![Showcase](https://i.imgur.com/ckEylez.gif)

## Install

`go get -u gitlab.com/viktomas/timer`

## Command reference

- `timer start [-a]` - starts a unit now
  - `-a` stands for append. Unit starts at the very time when last unit finished
- `timer pause` - pauses the unit until `timer start` is called
- `timer report` - shows a progress bar and finishes when unit finishes
- `timer delete` - deletes last unit
- `./reportAndNotify.sh` - reports and then triggers OSX notification

## User file

All units are stored as json in `~/.config/timer/timerData.json` (or in your home dir on Windows).

## Useful aliases

```sh
alias ts="timer start"
alias tp="timer pause"
alias tas="timer -a start"
alias tr="~/path/to/timer/reportAndNotify.sh"
```

## Acknowledgements

- Project logo is an icon from [FontAwesome](https://fontawesome.com/icons/stopwatch?style=solid)