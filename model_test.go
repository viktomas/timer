package main

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func ago(d time.Duration) time.Time {
	return time.Now().Add(-d)
}

func TestUnitIsRunning(t *testing.T) {
	unit := unit{Started: ago(time.Second), Duration: time.Minute}
	assert.True(t, unit.isRunning())
	assert.True(t, unit.isActive())
}

func TestUnitIsDone(t *testing.T) {
	unit := unit{Started: ago(time.Minute), Duration: time.Second}
	assert.False(t, unit.isActive())
}

func TestUnitIsPaused(t *testing.T) {
	unit := unit{
		Started:  ago(10 * time.Minute),
		Duration: 25 * time.Minute,
		Pauses:   []pause{{Start: ago(time.Minute)}},
	}
	assert.True(t, unit.isPaused())
	assert.True(t, unit.isActive())
	assert.False(t, unit.isRunning())
}

func TestUnitIsActiveWhenPauseDone(t *testing.T) {
	minuteAgo := ago(time.Minute)
	unit := unit{
		Started:  time.Now().Add(-10 * time.Minute),
		Duration: 25 * time.Minute,
		Pauses:   []pause{{Start: ago(2 * time.Minute), End: &minuteAgo}},
	}
	assert.True(t, unit.isRunning())
}

func TestGetLastUnitFromTheStore(t *testing.T) {
	unitA := unit{
		Started:  ago(50 * time.Minute),
		Duration: 25 * time.Minute,
	}
	unitB := unit{
		Started:  ago(10 * time.Minute),
		Duration: 25 * time.Minute,
	}
	store := store{&unitA, &unitB}
	result, ok := store.getLastUnit()
	assert.True(t, ok)
	assert.Equal(t, unitB, result)
}

func TestGetLastUnitFromEmptyStore(t *testing.T) {
	store := store{}
	_, ok := store.getLastUnit()
	assert.False(t, ok)
}

func TestIsEmpty(t *testing.T) {
	store := store{}
	assert.True(t, store.isEmpty())
}

func TestIsNotEmpty(t *testing.T) {
	store := store{{Started: time.Now()}}
	assert.False(t, store.isEmpty())
}
