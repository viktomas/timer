package main

import (
	"fmt"
	"os"
	"time"

	"github.com/vbauerster/mpb/v4"
	"github.com/vbauerster/mpb/v4/decor"
)

type command interface {
	execute(store) store
}

type pauseCommand struct{}
type startCommand struct {
	appendUnit        bool
	substractDuration time.Duration
}
type deleteLastCommand struct{}
type reportCommand struct{}

func (*pauseCommand) execute(store store) store {
	lastUnit, exists := store.getLastUnit()
	if !exists || !lastUnit.isRunning() {
		fmt.Println("no unit to be paused")
		return store
	}
	lastUnit.Pauses = append(lastUnit.Pauses, pause{Start: time.Now()})
	store.update(lastUnit)
	fmt.Printf("You just paused a unit\n---\n%v", lastUnit)
	return store
}

func (c *startCommand) execute(store store) store {
	startTime := time.Now().Add(-c.substractDuration)
	lastUnit, exists := store.getLastUnit()
	if exists {
		if lastUnit.isRunning() {
			fmt.Printf("Unit in progress\n---\n%v", lastUnit)
			return store
		}
		if lastUnit.isPaused() {
			fmt.Println("Unpausing unit")
			lastUnit.unpause()
			store.update(lastUnit)
			return store
		}
		if c.appendUnit {
			lastUnit, _ = store.getLastUnit()
			finishedAt := lastUnit.finishedAt()
			startTime = *finishedAt
		}
	}
	newUnit := unit{Started: startTime, Duration: defaultDuration}
	allUnits := append(store, &newUnit)
	fmt.Printf("You just have started\n---\n%v", newUnit)
	return allUnits
}

func (*deleteLastCommand) execute(store store) store {
	lastUnit, exists := store.getLastUnit()
	if !exists {
		fmt.Println("no unit to be deleted")
		return store
	}
	withoutLast := store[:len(store)-1]
	fmt.Printf("You just deleted the last unit\n---\n%v", lastUnit)
	return withoutLast
}

func (*reportCommand) execute(store store) store {
	lastUnit, exists := store.getLastUnit()
	if !exists {
		fmt.Println("the store is empty")
		return store
	}
	if lastUnit.isRunning() {
		progress(lastUnit, store.unitsToday())
		os.Exit(0)
	} else if lastUnit.isPaused() {
		fmt.Printf("There is a paused unit\n---\n%v", lastUnit)
	} else {
		fmt.Printf("Last unit ended %v ago\n", time.Now().Sub(*lastUnit.finishedAt()))
		fmt.Printf("Total amount of units today: %v\n", store.unitsToday())
	}
	return store
}

func progress(u unit, unitsToday int) {
	p := mpb.New(mpb.WithWidth(64))
	total := u.Duration.Milliseconds()
	startedWithAddedPauses := u.Started.Add(u.allPausesDuration())
	bar := p.AddBar(total,
		mpb.PrependDecorators(decor.Name(fmt.Sprintf("Unit #%v:", unitsToday))),
		mpb.AppendDecorators(decor.NewElapsed(decor.ET_STYLE_MMSS, startedWithAddedPauses)),
	)
	for u.isActive() {
		bar.SetCurrent(time.Since(startedWithAddedPauses).Milliseconds())
		time.Sleep(time.Duration(time.Second))
	}
	bar.SetCurrent(u.Duration.Milliseconds())
	// wait for our bar to complete and flush
	p.Wait()
}
