package main

import (
	"fmt"
	"time"
)

type store []*unit

type pause struct {
	Start time.Time  `json:"start"`
	End   *time.Time `json:"end"`
}

func (p pause) isFinished() bool {
	return p.End != nil
}

func (p pause) duration() time.Duration {
	end := time.Now()
	if p.End != nil {
		end = *p.End
	}
	return end.Sub(p.Start)
}

func (p *pause) finish() {
	now := time.Now()
	p.End = &now
}

type unit struct {
	Started  time.Time     `json:"started"`
	Duration time.Duration `json:"duration"`
	Pauses   []pause       `json:"pauses"`
}

func (u unit) String() string {
	return fmt.Sprintf(
		"Started %s\nDuration %v\nElapsed %v\n",
		u.Started.Format(time.Kitchen),
		u.Duration,
		time.Now().Sub(u.Started.Add(u.allPausesDuration())),
	)
}

func (u unit) isActive() bool {
	return (time.Since(u.Started) < u.Duration+u.allPausesDuration())
}

func (u unit) isRunning() bool {
	return u.isActive() && !u.isPaused()
}

func (u unit) allPausesDuration() time.Duration {
	var result time.Duration
	for _, p := range u.Pauses {
		result = result + p.duration()
	}
	return result
}

func (u unit) isPaused() bool {
	if len(u.Pauses) == 0 {
		return false
	}
	lastPause := u.Pauses[len(u.Pauses)-1]
	return !lastPause.isFinished()
}

func (u *unit) unpause() {
	if len(u.Pauses) == 0 {
		return
	}
	lastPause := u.Pauses[len(u.Pauses)-1]
	lastPause.finish()
	u.Pauses[len(u.Pauses)-1] = lastPause
}

func (u unit) finishedAt() *time.Time {
	if u.isPaused() {
		return nil
	}
	result := u.Started.Add(u.Duration + u.allPausesDuration())
	return &result
}

func (s store) getLastUnit() (unit, bool) {
	if s.isEmpty() {
		return unit{}, false
	}
	return *s[len(s)-1], true
}

// TODO Think about NonEmptyStore interface
func (s store) isEmpty() bool {
	return len(s) == 0
}

func (s store) unitsToday() int {
	total := 0
	year, month, day := time.Now().Date()
	startOfToday := time.Date(year, month, day, 0, 0, 0, 0, time.Now().Location())
	for _, u := range s {
		if startOfToday.Before(u.Started) {
			total++
		}
	}
	return total
}

func (s *store) update(newUnit unit) {
	for i, oldUnit := range *s {
		if oldUnit.Started.Equal(newUnit.Started) {
			(*s)[i] = &newUnit
			return
		}
	}
}
