package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"path"
	"time"
)

// the correct version is injected by `go build` command in release.sh script
var version = "master"

// TODO this should be configurable, otherwise tests can use this file by mistake
var filePath = ".config/timer/timerData.json"
var defaultDuration = 25 * time.Minute

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func getDataFilePath() string {
	homeDir, err := os.UserHomeDir()
	check(err)
	return path.Join(homeDir, filePath)
}

func loadUnits() (store, error) {
	_, err := os.Stat(getDataFilePath())
	if os.IsNotExist(err) {
		log.Printf("The task file %v doesn't exist, timer will create one\n", getDataFilePath())
		return []*unit{}, nil
	}
	check(err)
	data, err := ioutil.ReadFile(getDataFilePath())
	check(err)
	var result []*unit
	err = json.Unmarshal(data, &result)
	check(err)
	return result, nil
}

func saveUnits(u []*unit) error {
	file, _ := json.MarshalIndent(u, "", " ")
	return ioutil.WriteFile(getDataFilePath(), file, 0644)
}

func main() {
	execute()
}

func run(command command) {
	store, err := loadUnits()
	check(err)
	newStore := command.execute(store)
	err = saveUnits(newStore)
	check(err)
}
