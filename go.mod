module gitlab.com/viktomas/timer

go 1.13

require (
	github.com/spf13/cobra v1.0.0
	github.com/stretchr/testify v1.4.0
	github.com/vbauerster/mpb/v4 v4.11.2
)
