#!/bin/bash
timer report && osascript -e 'display notification "timer just finished a unit, take a rest" with title "Unit finished"' && afplay /System/Library/Sounds/Glass.aiff
