package main

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func happenedAlmostNow(check time.Time) bool {
	return check.After(time.Now().Add(-time.Millisecond)) && check.Before(time.Now().Add(time.Millisecond))
}

func TestPauseCommand(t *testing.T) {
	unit := unit{
		Started:  ago(10 * time.Minute),
		Duration: 25 * time.Minute,
	}
	store := store{&unit}
	var pause pauseCommand
	newStore := pause.execute(store)
	newUnit, _ := newStore.getLastUnit()
	assert.True(t, newUnit.isPaused())
	assert.True(t, happenedAlmostNow(newUnit.Pauses[0].Start))
}

func TestPauseCommandCannotPause(t *testing.T) {
	unit := unit{
		Started:  ago(10 * time.Minute),
		Pauses:   []pause{{Start: time.Now()}},
		Duration: 25 * time.Minute,
	}
	store := store{&unit}
	var pause pauseCommand
	assert.Equal(t, store, pause.execute(store))
}

func TestPauseOnEmptyStore(t *testing.T) {
	var pause pauseCommand
	assert.Equal(t, store{}, pause.execute(store{}))
}

func TestStartCommand(t *testing.T) {
	start := startCommand{false, 0}
	newStore := start.execute(store{})
	newUnit, _ := newStore.getLastUnit()
	assert.True(t, newUnit.isRunning())
	assert.True(t, happenedAlmostNow(newUnit.Started))
}

func TestStartInPastCommand(t *testing.T) {
	start := startCommand{false, 10 * time.Minute}
	newStore := start.execute(store{})
	newUnit, _ := newStore.getLastUnit()
	assert.True(t, newUnit.isRunning())
	assert.True(t, happenedAlmostNow(newUnit.Started.Add(10*time.Minute)))
}

func TestStartUnpauseCommand(t *testing.T) {
	unit := unit{
		Started:  ago(10 * time.Minute),
		Pauses:   []pause{{Start: ago(5 * time.Minute)}},
		Duration: 25 * time.Minute,
	}
	store := store{&unit}
	start := startCommand{false, 0}
	newStore := start.execute(store)
	newUnit, _ := newStore.getLastUnit()
	assert.True(t, newUnit.isRunning())
	assert.True(t, happenedAlmostNow(*newUnit.Pauses[0].End))
}

func TestStartUnitInProgress(t *testing.T) {
	unit := unit{
		Started:  ago(10 * time.Minute),
		Duration: 25 * time.Minute,
	}
	store := store{&unit}
	start := startCommand{false, 0}
	assert.Equal(t, store, start.execute(store))
}

func TestStartUnitAppend(t *testing.T) {
	unit := unit{
		Started:  ago(30 * time.Minute),
		Duration: 25 * time.Minute,
	}
	store := store{&unit}
	start := startCommand{true, 0}
	newStore := start.execute(store)
	newUnit, _ := newStore.getLastUnit()
	assert.True(t, newUnit.Started.Before(ago(5*time.Minute)))
}

func TestDeleteCommand(t *testing.T) {
	unit := unit{
		Started:  ago(30 * time.Minute),
		Duration: 25 * time.Minute,
	}
	delete := deleteLastCommand{}
	newStore := delete.execute(store{&unit})
	assert.Equal(t, store{}, newStore)
}
