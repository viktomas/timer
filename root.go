package main

import (
	"fmt"
	"os"
	"regexp"
	"strconv"
	"time"

	"github.com/spf13/cobra"
)

var (
	appendFlag bool

	rootCmd = &cobra.Command{
		Use:   "timer",
		Short: "Simple time-tracking CLI app",
		Long: `Timer is a golang tool that allows simple time tracking.
It allows to measure time in 25min intervals loosely following
the pomodoro technique`,
	}

	reportCmd = &cobra.Command{
		Use:   "report",
		Short: "report total units today and the unit in progress",
		Run: func(cmd *cobra.Command, args []string) {
			run(&reportCommand{})
		},
	}
	startCmd = &cobra.Command{
		Use:   "start",
		Short: "start a new unit",
		Run: func(cmd *cobra.Command, args []string) {
			substract := parseSubstractDuration(args)
			run(&startCommand{appendFlag, substract})
		},
	}
	pauseCmd = &cobra.Command{
		Use:   "pause",
		Short: "pause running unit",
		Run: func(cmd *cobra.Command, args []string) {
			run(&pauseCommand{})
		},
	}
	deleteCmd = &cobra.Command{
		Use:   "delete",
		Short: "delete the last unit",
		Run: func(cmd *cobra.Command, args []string) {
			run(&deleteLastCommand{})
		},
	}
)

// Execute executes the root command.
func execute() error {
	return rootCmd.Execute()
}

func init() {
	startCmd.Flags().BoolVarP(&appendFlag, "append", "a", false, "should append the new unit to previous one")

	rootCmd.AddCommand(reportCmd)
	rootCmd.AddCommand(startCmd)
	rootCmd.AddCommand(pauseCmd)
	rootCmd.AddCommand(deleteCmd)
}

func er(msg interface{}) {
	fmt.Println("Error:", msg)
	os.Exit(1)
}

func parseSubstractDuration(args []string) time.Duration {
	if len(args) < 1 {
		return 0
	}
	if matched, _ := regexp.MatchString(`-\d+`, args[0]); !matched {
		return 0
	}
	var minutes int64
	minutes, err := strconv.ParseInt(args[0][1:], 10, 64)
	if err != nil {
		return 0
	}
	return time.Duration(minutes) * time.Minute
}
