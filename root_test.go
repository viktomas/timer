package main

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestParseSubstractDuration(t *testing.T) {
	var zero time.Duration
	assert.Equal(t, 10*time.Minute, parseSubstractDuration([]string{"-10"}))
	assert.Equal(t, zero, parseSubstractDuration([]string{"10"}))
	assert.Equal(t, zero, parseSubstractDuration([]string{"+10"}))
	assert.Equal(t, zero, parseSubstractDuration([]string{"abc"}))
}
